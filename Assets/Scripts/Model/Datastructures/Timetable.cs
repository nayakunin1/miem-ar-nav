﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timetable : MonoBehaviour
{
        private string type;
        private string lecturer;
        private string ruzID;
        private string name;
        private string dateStart;
        private string timeStart;
        private string dateEnd;
        private string discipline;
        



        private Timetable() { }

        
        public Timetable(string type, string lecturer, string ruzID, string name, string dateStart, string timeStart, string dateEnd, string discipline)
        {
            Type = type;
            Lecturer = lecturer;
            RuzID = ruzID;
            Name = name;
            DateStart = dateStart;
            TimeStart = timeStart;
            DateEnd = dateEnd;
            Discipline = discipline;
          
        }

        public bool Contains(string text)
        {
            var contains = false;
            if (Name.Contains(text))
                contains = true;

            return contains;
        }

        public string DateEnd
        {
            get { return dateEnd; }
            set { dateEnd = value; }
        }
        public string Discipline
        {
            get { return discipline; }
            set { discipline = value; }
        }
        public string RuzID
        {
            get { return ruzID; }
            set { ruzID = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string DateStart
        {
            get { return dateStart; }
            set { dateStart = value; }
        }

        public string TimeStart
        {
        get { return timeStart; }
        set { timeStart = value; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        public string Lecturer
        {
            get { return lecturer; }
            set { lecturer = value; }
        }

}
