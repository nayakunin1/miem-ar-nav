﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class RuzAPIConnector : MonoBehaviour
{
    public MarkerDetection _MarkerDetection;
    public RoomDatabase _RoomDatabase;

    public const string baseURL = "https://api.hseapp.ru/ruz/lessons?start={0}&offset={1}&auditorium={2}";
    private Text responseText;


    // Список пар должен идти сюда
    private List<Timetable> classesList = new List<Timetable>();


    private string currentDate; // Дата с которой запрашивается расписание
    private string offset; // На сколько дней вперед
    private string auditorium; // В какой аудитории

    private bool receivedResult = false;


    public void Request(string roomName)
    {
        classesList.Clear();

        currentDate = System.DateTime.Now.ToString("yyyy-MM-dd");
        Debug.Log("Current date " + currentDate);
        offset = "2";

        // Записываем ID аудитории в 
        auditorium = _RoomDatabase.GetRoom(roomName).RuzID;
        //Debug.Log("RuzID of " + roomName + "Classroom is " + auditorium)        
        currentDate = "2020-04-07";

        // Получаем строку в нужном формате
        string URL = string.Format(baseURL, currentDate, offset, auditorium);
        Debug.Log("Request URL = " + URL);
        WWW request = new WWW(URL);
        StartCoroutine(OnResponse(request));
    }

    private IEnumerator OnResponse(WWW req)
    {
        receivedResult = false;
        // Ожидаем ответ от сервера
        yield return req;

        if (string.IsNullOrEmpty(req.error))
        {
            receivedResult = true;

            Debug.Log("Parsing Data");
            Debug.Log("RUZ API Text: " + req.text.Replace("\n", "").Replace(" ", ""));

            Debug.Log("Response (no parsed):\n" + req);
            var response = JSON.Parse(req.text).AsArray;
            Debug.Log("Parsed response:\n");

            foreach (var classPair in response)
            {
                var type = classPair.Value["type"];
                var lecturer = classPair.Value["lecturer"];
                var ruzID = classPair.Value["auditorium_id"];
                var roomName = classPair.Value["auditorium"];

                var dateStart = UnixTimeToDateTime(classPair.Value["date_start"]).ToLocalTime().ToString("dddd d MMMM");
                var timeStart = UnixTimeToDateTime(classPair.Value["date_start"]).ToString("HH:mm");
                var dateEnd = UnixTimeToDateTime(classPair.Value["date_end"]).ToString("HH:mm");
                var discipline = classPair.Value["discipline"];

                Debug.Log("Created Class instance " + discipline);

                if (roomName == null || ruzID == null || ruzID == null || dateStart == null)
                {
                    Debug.Log("Error Calculating Room KeyValuePair: " + classPair);
                    continue;
                }

                var newTimetable = new Timetable(type, lecturer, ruzID, roomName, dateStart, timeStart, dateEnd, discipline);

                Debug.Log("Created Class instance " + dateStart + "\n" + discipline);
                classesList.Add(newTimetable);
                //Debug.Log("Room: " + roomName + " Floor: " + floorNumber);
            }

            _MarkerDetection.ReceiveClassesList(classesList);


        }
        else
        {
            Debug.Log("Error: " + req.error);
        }


    }

    /// <summary>
    /// Convert Unix time value to a DateTime object.
    /// </summary>
    /// <param name="unixtime">The Unix time stamp you want to convert to DateTime.</param>
    /// <returns>Returns a DateTime object that represents value of the Unix time.</returns>
    public System.DateTime UnixTimeToDateTime(long unixtime)
    {
        System.DateTime dtDateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddMilliseconds(unixtime).ToLocalTime();
        return dtDateTime;
    }
}
