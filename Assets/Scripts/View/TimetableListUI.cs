﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimetableListUI : MonoBehaviour
{
    public GameObject _ClassTemplate;
    public GameObject _TimetableButton;
    public NavigationPresenter _NavigationPresenter;
    public SystemStatePresenter _SystemStatePresenter;

    private List<GameObject> classesGameobjects = new List<GameObject>();

    // Update is called once per frame

    /// <summary>
    /// 
    /// </summary>
    /// <param name="room"></param>
    ///

    public void SendClassesList(List<Timetable> classesList)
    {
        ClearClassesList();
        _TimetableButton.SetActive(true);

        foreach (var classInstance in classesList)
        {
            GenClasses(classInstance);
        }
        SortListByDatestart();
    }
    private void GenClasses(Timetable timetable)
    {
        GameObject classInstance = Instantiate(_ClassTemplate) as GameObject;
        classInstance.SetActive(true);
        classInstance.GetComponent<TimetableButton>().InitializeButton(timetable);
        classInstance.name = timetable.Name;
        classInstance.transform.SetParent(_ClassTemplate.transform.parent, false); // set false so that button doesn't position themselves in worldspace. Makes it more dynamic
        classesGameobjects.Add(classInstance);
    }

    private void ClearClassesList()
    {
        foreach (var classInstance in classesGameobjects)
        {
            GameObject.Destroy(classInstance);
        }
        classesGameobjects.Clear();
    }
    /**
     * Go through each room in the list and hides the button if they don't contain the text
     * No consideration for performance
     */
    public void SearchRoom(string text)
    {
        foreach (var button in classesGameobjects)
        {
            if (button.GetComponent<TimetableButton>().ContainsText(text) == false)
                button.SetActive(false);
            else
                button.SetActive(true);
        }
    }

    /**
     * Makes all buttons visible again
     */
    public void ResetAllButton()
    {
        foreach (var button in classesGameobjects)
        {
            button.SetActive(true);
        }
    }

    /**
     * Sorts the room buttons by changing their order as siblings
     * Can be used to order them by their distance
     */
    public void SortList(System.Comparison<GameObject> comparison)
    {
        classesGameobjects.Sort(comparison);
        foreach (var classInstance in classesGameobjects)
        {
            /**
             * Set as last so that the next element in the list comes after the first one;
             * Sorted list: 1, 2, 3, 4 
             * {} -> {1} -> {1,2} -> {1,2,3}
             */

            classInstance.transform.SetAsLastSibling();
        }
    }

 
    public void SortListByDatestart()
    {
        SortList(SortByDatestartComparator);
    }

 
    private int SortByDatestartComparator(GameObject classInstance1, GameObject classInstance2)
    {
        string date1 = classInstance1.GetComponent<TimetableButton>().GetDateStart();
        string date2 = classInstance2.GetComponent<TimetableButton>().GetDateStart();
        return date1.CompareTo(date2);
    }

}
