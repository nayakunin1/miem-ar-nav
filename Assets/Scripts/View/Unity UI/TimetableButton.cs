﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TimetableButton: MonoBehaviour
{
    public TimetableListUI _TimetableListUI;
    
    public TMP_Text _RoomNameText;
    public TMP_Text _DateStartText;
    public TMP_Text _TimeStartText;
    public TMP_Text _DateEndText;
    public TMP_Text _LecturerText;
    public TMP_Text _TypeText;
    public TMP_Text _DisciplineText;


    private string allText;

    
    private Timetable timetable;
    
    public void InitializeButton(Timetable timetable)
    {
        this.timetable = timetable;
        _RoomNameText.text = timetable.Name;
        _DateStartText.text = timetable.DateStart;
        _TimeStartText.text = timetable.TimeStart;
        _DateEndText.text = timetable.DateEnd;
        _LecturerText.text = timetable.Lecturer;
        _TypeText.text = timetable.Type;
        _DisciplineText.text = timetable.Discipline;


        allText = "Classroom " + _RoomNameText.text + "\n" +
            _TypeText.text + " " +  _DisciplineText.text + "\n" + _LecturerText.text;

    }

    // Returns true if the room botton contains the text
    public bool ContainsText(string text)
    {
        return allText.ToLower().Contains(text.ToLower());
    }

    public string GetName()
    {
        return timetable.Name;
    }

    /// <summary>
    /// For Sorting in TimetableListUI
    /// </summary>
    /// <returns></returns>
    public string GetDateStart()
    {
        return timetable.DateStart;
    }
}

